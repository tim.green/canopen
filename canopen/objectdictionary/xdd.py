import lxml.etree as et  #lxml handles the default namespace whereas xml.etree.ElementTree requires complex workarounds.  lxml also has xpath
import logging
from canopen import objectdictionary
from canopen.objectdictionary import datatypes as dt
import re

# Some general documentation and notes:
# complex data types are not currently supported.  i.e. a <parameter> element with a <dataTypeIDRef> subelement is not supported.
# if there is no Unique id in the object/subobject, then the attributes come from the obj/subobj
# otherwise they come from the parameter element, per the CiA311 spec:
# Unique ID of an appropriate element in the application
# process part referenced from this sub-object; if the
# attribute uniqueIDRef is present, the attributes name,
# dataType, lowLimit, highLimit, accessType, defaultValue,
# actualValue and denotation shall not be present.


# TODO: pass in a language code when building object, so that we get the right language labels
# TODO: the supportedLanguages attribute of the ProfileBody element identifies supported languages
# TODO: for object_type = 8 and 9, the subNumber attribute of CANopenObject should match the number of CANopenSubObjects

logger = logging.getLogger(__name__)

XDD_EDS_ACCESS_TYPE_MAP = {
    'const': 'const',
    'read': 'ro',
    'write': 'wo',
    'readWrite': 'rw',
    'readWriteInput': 'rw',
    'readWriteOuput': 'rw',
    'noAccess': ''

}

# xdd types follow IEC_61131-3: https://en.wikipedia.org/wiki/IEC_61131-3
# for now will not map types from xdd that don't have corresponding data dictionary type as defined in CiA spec 301
XDD_EDS_DATA_TYPE_MAP = {
    "BOOL": dt.BOOLEAN,
    "BITSTRING": None,
    "BYTE": None,
    "CHAR": None,
    "WORD": None,
    "DWORD": None,
    "LWORD": None,
    "SINT": dt.INTEGER8,
    "INT": dt.INTEGER16,
    "DINT": dt.INTEGER32,
    "LINT": dt.INTEGER64,
    "USINT": dt.UNSIGNED8,
    "UINT": dt.UNSIGNED16,
    "UDINT": dt.UNSIGNED32,
    "ULINT": dt.UNSIGNED64,
    "REAL": dt.REAL32,
    "LREAL": dt.REAL64,
    "STRING": None,
    "WSTRING": None
}

# object types
NULL =      0x00
DOMAIN =    0x02
DEFTYPE =   0x05
DEFSTRUCT = 0x06
VAR =       0x07
ARR =       0x08
RECORD =    0x09


def xpath_types():
    # make an xpath expression of mapped xdd datatypes
    xpath_type_template = 'self::d:{}'
    xpath_types = '*[{}]'
    return xpath_types.format(' or '.join([xpath_type_template.format(k) for (k, v) in XDD_EDS_DATA_TYPE_MAP.items() if v]))


XPATH_TYPES = xpath_types()


def import_xdd(xdd):
    """Import an XDD file.

    :param xdd:
        Either a path to an xdd-file, a file-like object, or an instance of
        :class:`lxml.etree.Element`.

    :returns:
        The Object Dictionary.
    :rtype: canopen.ObjectDictionary
    """
    od = objectdictionary.ObjectDictionary()
    if et.iselement(xdd):
        tree = xdd
    else:
        tree = et.parse(xdd).getroot()

    nsmap = Xdd.nsmap_default(tree.nsmap)

    # xdd supports multiple baud rates, but ObjectDictionary supports one.  So we will just use the first one.
    supported_baud_rate = tree.xpath(
        './/d:ISO15745Profile/d:ProfileBody[@xsi:type="ProfileBody_CommunicationNetwork_CANopen"]/d:TransportLayers/d:PhysicalLayer/d:baudRate/d:supportedBaudRate/@value',
        namespaces=nsmap)[0]

    (rate, units) = supported_baud_rate.split(' ')
    od.bitrate = int(rate) * 1000

    node_id_element = tree.xpath(
        './/d:ISO15745Profile/d:ProfileBody[@xsi:type="ProfileBody_CommunicationNetwork_CANopen"]/d:NetworkManagement/d:deviceCommissioning/@nodeID',
        namespaces=nsmap)[0]
    od.node_id = int(node_id_element)

    objects = tree.xpath('.//d:ISO15745Profile/d:ProfileBody[@xsi:type="ProfileBody_CommunicationNetwork_CANopen"]/d:ApplicationLayers/d:CANopenObjectList/d:CANopenObject', namespaces=nsmap)
    for object_element in objects:
        od.add_object(CANopenObject(od.node_id, object_element).object)

    return od


class Xdd(object):
    @staticmethod
    def nsmap_default(nsmap):
        """
        # add a key for the default namespace that can be used in xpath expressions, and remove the default one
        :param nsmap:
        :return: dict containing the namespace map
        """

        # print(nsmap)
        # add a key for the default namespace that can be used in xpath expressions, and remove the default one
        default_ns = nsmap.pop(None, None)

        # print(default_ns)
        nsmap['d'] = default_ns
        # nsmap['re'] = "http://exslt.org/regular-expressions"  # add regex namespace so we can use regex in xpath
        # print(nsmap)
        return nsmap


class CANopenObject(object):

    def __init__(self, node_id, object_element, index=None):
        """
        create instance by passing in an element reference to a <CANopenObject> element and the object's index
        :param node_id: the node_id for this CANopenObject
        :param object_element: lxml element reference to a <CANopenObject> element
        :param index: index of the object.  if None, it will be read from the xml.  Use this when passing a CANopenSubObject element as object_element
        """
        self.node_id = node_id
        self.object_element = object_element # reference to the xml element
        self._nsmap = Xdd.nsmap_default(object_element.nsmap)

        # define properties of variables, arrays, and records.  Not all are applicable to each type
        self._parameter_element = None # xml element for the parameter that matches the objects uniqueID
        self._has_unique_id = None # whether the object/subobject has a uniqueID referencing a parameter element
        self._unique_id = None
        self._index = index
        self._sub_index = None
        self._object_type = None  # the object type
        self._name = None
        self._data_type = None
        self._unit = None
        self._access_type = None
        self._default_value = None
        self._description = None
        self._actual_value = None
        self._low_limit = None
        self._high_limit = None
        self._offset = None
        self._factor = None

        self._object = None  # the actual object

    @staticmethod
    def _int(value, base):
        """
        convert a string to an int, accounting for odd formats like a hex value written as 0005 or 01
        vector CANeds converts datatypes with up to 3 leading zeroes, but they are hex.
            subindexes have one leading zero, but they need to be treated as hex and not octal.
        in python, base 10 do not start with a zero: 1, 10, 25
        base 8 start with one zero: 01, 012, 031
        base 16 start with 0x: 0x1, 0xA, 0x19
        :param value: the string value to convert to int
        :param base: the base to use
        :return: int
        """
        value = value.strip()
        if base > 0:
            # no guessing, we'll assume the caller knows what they are doing
            return int(value, base)
        elif value == '00' or value == '0':
            return 0
        else:
            # we are supposed to guess
            # if value starts with  "0." or "." we'll assume it a floating point base 10.
            regex = '^0?\.[0-9]*'
            if re.match(regex, value):
                return float(value)

            # if value starts with at least one zero not followed by an x, we'll assume it's hex and add 0x to the front
            regex = '^0(?!x)0*'
            if re.match(regex, value):
                value = '0x'+value

            return int(value, 0) # the default is to treat it as formatted properly for it's base encoding

    @staticmethod
    def _escape_namespace(namespace):
        """
        take a namespace and escape regex special chars
        :param namespace: the namespace to escape
        :return: str
        """
        reserved = ['\\', '.', '^', '$', '*', '+', '?', '(', ')', '[', '{', '|']
        v = namespace
        for char in reserved:
            v = v.replace(char, '\\'+char)
        return v

    def _convert_value(self, value):
        """
        converts a value by doing $NODEID placeholder substitution and converting to int
        :param value: the value to convert from string representation
        :return:
        """
        # TODO: handle float types
        # TODO: what if the placeholder is thus: $NODEID + ... (with a space before plus sign)
        if value is None:
            return None

        if '$NODEID+' in value and self.node_id is not None:
            return self._int(value.replace('$NODEID+', ''), 0) + self.node_id
        else:
            return self._int(value, 0)

    def _xpath_first(self, element, xpath):
        # return the first element of an xpath expression, or None if the xpath returns no nodes
        obj_list = element.xpath(xpath, namespaces=self._nsmap)
        return obj_list[0] if len(obj_list) > 0 else None

    def _remove_ns_from_tag_name(self, tag):
        # remove any namespace from the beginning of the tag name
        # regex should end up looking something like ^{(http://www\.w3\.org/2001/XMLSchema-instance|http://www\.canopen\.org/xml/1\.0)}
        regex = '^{{({})}}'.format('|'.join([self._escape_namespace(v) for (k, v) in self._nsmap.items()]))
        return re.sub(regex, '', tag)

    @property
    def has_unique_id(self):
        """
        whether the object/subobject has a uniqueID referencing a parameter element
        :return: boolean
        """
        if self._has_unique_id is None:
            self._has_unique_id = False if self.unique_id is None else True

        return self._has_unique_id

    @property
    def unique_id(self):
        """
        get the unique id of the object element
        lazy loading
        :return: str
        """
        if self._unique_id is None:
            self._unique_id = self._xpath_first(self.object_element, '@uniqueIDRef')

        return self._unique_id

    @property
    def index(self):
        """
        takes object tree and unique id and returns tuple of (index, subindex)
        subindex will be None if there isn't one for the unique id passed
        lazy loading
        :return: int
        """
        if self._index is None:
            index = self._xpath_first(self.object_element, '@index')
            self._index = self._int(index, 16)

        return self._index

        # # code for getting sub index
        # subindex = xpath_first(object_element_tuple[1], '@subIndex', nsmap)
        # index = xpath_first(object_element_tuple[1], 'parent::*/@index', nsmap)
        # return (int(index, 16), int(subindex, 16))

    @property
    def parameter_element(self):
        """
        get the xml parameter element that matches the object_elements uniqueID
        lazy loading
        :return: Element
        """
        if self._parameter_element is None:
            unique_id_xpath = './/d:ISO15745Profile/d:ProfileBody[@xsi:type="ProfileBody_Device_CANopen"]/d:ApplicationProcess/d:parameterList/d:parameter[@uniqueID="{}"]'.format(self.unique_id)
            root = self.object_element.getroottree().getroot()
            self._parameter_element = self._xpath_first(root, unique_id_xpath)

        return self._parameter_element

    @property
    def object_type(self):
        #TODO: test hex versus decimal form of the value
        """
        get the object type of the canopen object
        :return: int
        """
        if self._object_type is None:
            object_type = self._xpath_first(self.object_element, '@objectType')
            object_type = int(object_type, 0) if object_type is not None else 0
            self._object_type = object_type

        return self._object_type

    @property
    def name(self):
        """
        get the name of the object
        if the object has a unique id, the value comes from the parameter element, otherwise it comes from the object element
        :return: str
        """
        if self._name is None:
            if self.has_unique_id:
                self._name = self._xpath_first(self.parameter_element, 'd:label[@lang="en"]/text()')
            else:
                self._name = self._xpath_first(self.object_element, '@name')

        return self._name

    @property
    def data_type(self):
        #TODO: test hex versus decimal form of the value
        """
        get the data type of the object
        if the object has a unique id, the value comes from the parameter element, otherwise it comes from the object element
        :return: int
        """
        if self._data_type is None:
            if self.has_unique_id:
                data_type = self._remove_ns_from_tag_name(self._xpath_first(self.parameter_element, XPATH_TYPES).tag)
                self._data_type = XDD_EDS_DATA_TYPE_MAP[data_type]
            else:
                self._data_type = self._int(self._xpath_first(self.object_element, '@dataType'), 0)

        return self._data_type

    @property
    def unit(self):
        """
        return unit from the parameter element, if the object is linked to a parameter
        :return: str
        """
        if self._unit is None:
            if self.has_unique_id:
                self._unit = self._xpath_first(self.parameter_element, 'd:unit/d:label[@lang="en"]/text()')

        if self._unit is None:
            self._unit = ''

        return self._unit

    @property
    def access_type(self):
        """
        return access type, mapped to the EDS representation if applicable
        if the object has a unique id, the value comes from the parameter element, otherwise it comes from the object element
        :return: str
        """
        if self._access_type is None:
            if self.has_unique_id:
                self._access_type = XDD_EDS_ACCESS_TYPE_MAP[self._xpath_first(self.parameter_element, '@access')]
            else:
                self._access_type = self._xpath_first(self.object_element, '@accessType')

        return self._access_type

    @property
    def default_value(self):
        """
        return default value, converted to int
        if the object has a unique id, the value comes from the parameter element, otherwise it comes from the object element
        :return: int
        """
        if self._default_value is None:
            if self.has_unique_id:
                self._default_value = self._convert_value(self._xpath_first(self.parameter_element, 'd:defaultValue/@value'))
            else:
                self._default_value = self._convert_value(self._xpath_first(self.object_element, '@defaultValue'))

        return self._default_value

    @property
    def description(self):
        """
        return the description (using the denotation attribute/element for this)
        if the object has a unique id, the value comes from the parameter element, otherwise it comes from the object element
        :return: str
        """
        if self._description is None:
            if self.has_unique_id:
                self._description = self._xpath_first(self.parameter_element, 'd:denotation/d:label[@lang="en"]/text()')
            else:
                self._description = self._xpath_first(self.object_element, '@denotation')

        if self._description is None:
            self._description = ''

        return self._description

    @property
    def actual_value(self):
        """
        the value as defined in the dictionary
        if the object has a unique id, the value comes from the parameter element, otherwise it comes from the object element
        :return: int
        """
        if self._actual_value is None:
            if self.has_unique_id:
                self._actual_value = self._convert_value(self._xpath_first(self.parameter_element, 'd:actualValue/@value'))
            else:
                self._actual_value = self._convert_value(self._xpath_first(self.object_element, '@actualValue'))

        return self._actual_value

    def _get_low_limit_from_parameter(self):
        """
        This parses the zero-to-many subelements of the allowedValues element to get the lowest value
        Lowest and highest can be contained in either a <value> or <range> subelement.  All must be examined to find
        the smallest and largest values.
        :return: int
        """

        min_val = None

        # parse elements that can have a minimum value
        # d:allowedValues/d:range/d:minValue/@value
        # d:allowedValues/d:value/@value
        xpath = "d:allowedValues//*[@value and (self::d:minValue or self::d:value)]/@value"
        for value in self.parameter_element.xpath(xpath, namespaces=self._nsmap):
            value = self._convert_value(value)
            if not min_val:
                min_val = value
            else:
                min_val = value if value < min_val else min_val

        return min_val

    def _get_high_limit_from_parameter(self):
        """
        This parses the zero-to-many subelements of the allowedValues element to get the lowest value
        Lowest and highest can be contained in either a <value> or <range> subelement.  All must be examined to find
        the smallest and largest values.
        :return: int
        """

        max_val = None

        # parse elements that can have a minimum value
        # d:allowedValues/d:range/d:minValue/@value
        # d:allowedValues/d:value/@value
        xpath = "d:allowedValues//*[@value and (self::d:maxValue or self::d:value)]/@value"
        for value in self.parameter_element.xpath(xpath, namespaces=self._nsmap):
            value = self._convert_value(value)
            if not max_val:
                max_val = value
            else:
                max_val = value if value > max_val else max_val

        return max_val

    @property
    def low_limit(self):
        """
        return lower limit from the object element
        :return: int
        """
        if self._low_limit is None:
            if self.has_unique_id:
                self._low_limit = self._get_low_limit_from_parameter()
            else:
                self._low_limit = self._convert_value(self._xpath_first(self.object_element, '@lowLimit'))

        return self._low_limit

    @property
    def high_limit(self):
        """
        return upper limit from the object element
        :return: int
        """
        if self._high_limit is None:
            if self.has_unique_id:
                self._high_limit = self._get_high_limit_from_parameter()
            else:
                self._high_limit = self._convert_value(self._xpath_first(self.object_element, '@highLimit'))
        return self._high_limit

    @property
    def sub_index(self):
        """
        get the value of subindex from the object node
        if the object node is not a CANopenSubObject, then this will continue to look in the xml for the value and return None
        :return: int
        """
        if self._sub_index is None:
            sub_index = self._xpath_first(self.object_element, '@subIndex')
            self._sub_index = self._int(sub_index, 0) if sub_index is not None else 0

        return self._sub_index

    @property
    def offset(self):
        """
        return the offset
        if the object has a unique id, the value comes from the parameter element, otherwise it's defaulted to 0
        :return: str
        """
        if (self._offset is None) and self.has_unique_id:
            self._offset = self._convert_value(self._xpath_first(self.parameter_element, '@offset'))

        if self._offset is None: # parameter element didn't specify offset
            self._offset = 0

        return self._offset

    @property
    def factor(self):
        """
        return the factor
        if the object has a unique id, the value comes from the parameter element, otherwise it's defaulted to 1
        :return: str
        """
        if (self._factor is None) and self.has_unique_id:
            self._factor = self._convert_value(self._xpath_first(self.parameter_element, '@multiplier'))

        if self._factor is None: # parameter element didn't specify factor
            self._factor = 1

        return self._factor

    @property
    def object(self):
        """
        return either a Variable, Record, or Array typed object depending on the contents of the self.object_element
        lazy building of the object
        :return: Element
        """

        if self._object is None:
            child_count = len(self.object_element.getchildren())

            if self.object_type in (VAR, DOMAIN):
                if child_count > 0:
                    e = SyntaxError("Object is VAR or DOMAIN, but child nodes found")
                    e.lineno=self.object_element.sourceline
                    raise e

                var = objectdictionary.Variable(self.name, self.index, self.sub_index)

                var.default = self.default_value
                var.unit = self.unit
                var.access_type = self.access_type
                var.data_type = self.data_type
                var.description = self.description
                var.value = self.actual_value
                var.min = self.low_limit
                var.max = self.high_limit
                var.offset = self.offset
                var.factor = self.factor

                self._object = var
            elif self.object_type in (ARR, RECORD):
                if self.object_type == ARR:
                    obj = objectdictionary.Array(self.name, self.index)
                else:
                    obj = objectdictionary.Record(self.name, self.index)

                sub_objects = self.object_element.xpath('d:CANopenSubObject', namespaces=self._nsmap)
                for sub_object_element in sub_objects:
                    obj.add_member(CANopenObject(self.node_id, sub_object_element, index=self.index).object)

                self._object = obj

        return self._object