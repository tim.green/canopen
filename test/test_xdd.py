import canopen
from canopen import objectdictionary
from parameterized import parameterized
from test import xdd_test_dict as td
from test import can_open_unit_test_case as t

#TODO:
# test baud rate of "auto-baudRate"
# consider validating the xdd against the xsd


class TestImportXdd(t.CanOpenUnitTestCase):

    def setUp(self):
        self.od = canopen.import_od(td.XDD_PATH, 5)

    def test_load_file_object(self):
        self.assertEquals(self.od.bitrate, 250000)
        self.assertEquals(self.od.node_id, 5)

        self.assertEquals(len(self.od), len(td.SAMPLE_XDD_EXPECTED), "object dictionary has incorrect length")

    def test_dictionary(self):
        # make sure all variables in the object dictionary are in the test dictionary

        for index in self.od:
            test_entries = [x for x in td.SAMPLE_XDD_EXPECTED if x[2]['index'] == index]
            self.assertEqual(1, len(test_entries), 'number of matching test entries for index 0x{:02X} is not 1'.format(index))

            variable = self.od[index]

            self.assert_xdd_object_equals(variable, *test_entries[0])

    @parameterized.expand(td.SAMPLE_XDD_EXPECTED)
    def test_object(self, object_element_index, object_class, value_dict):
        # make sure all variables in the test dict are in the object dictionary

        variable = self.od[value_dict['index']]
        self.assert_xdd_object_equals(variable, object_element_index, object_class, value_dict)
