import unittest
from canopen import objectdictionary

class CanOpenUnitTestCase(unittest.TestCase):

    def assert_xdd_object_equals(self, object, object_element_index, object_class, value_dict):
        self.assertIsInstance(object, object_class, 'class instance wrong for test array index {}'.format(object_element_index))
        self.assertEquals(object.name, value_dict['name'], 'name wrong for test array index {}'.format(object_element_index))
        self.assertEquals(object.index, value_dict['index'], 'index wrong for test array index {}'.format(object_element_index))

        if type(object) is objectdictionary.Variable:
            self.assert_xdd_variable_equals(object, value_dict, 'test array index {}'.format(object_element_index))

        elif type(object) in [objectdictionary.Array, objectdictionary.Record]:
            self.assert_xdd_arr_or_rec_equals(object, value_dict, object_element_index)

        else:
            self.assert_(False, 'untested object type encountered in test')

    def assert_xdd_variable_equals(self, variable, value_dict, location_identifiers):
        self.assertEquals(variable.subindex, value_dict['subindex'],
                          'subindex wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.default, value_dict['default'], 'default wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.unit, value_dict['unit'], 'unit wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.factor, value_dict['factor'], 'factor wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.offset, value_dict['offset'], 'offset wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.min, value_dict['min'], 'min wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.max, value_dict['max'], 'max wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.value, value_dict['value'], 'value wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.data_type, value_dict['data_type'],
                          'data_type wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.access_type, value_dict['access_type'],
                          'access_type wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.description, value_dict['description'],
                          'description wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.value_descriptions, value_dict['value_descriptions'],
                          'value_descriptions wrong for {}'.format(location_identifiers))
        self.assertEquals(variable.bit_definitions, value_dict['bit_definitions'],
                          'bit_definitions wrong for {}'.format(location_identifiers))

    def assert_xdd_arr_or_rec_equals(self, arr_or_rec, value_dict, object_element_index):
        self.assertEquals(len(arr_or_rec.subindices), len(value_dict['subindices']),
                          'subindices length wrong for test array index {}'.format(object_element_index))
        self.assertEquals(len(arr_or_rec.names), len(value_dict['names']),
                          'names length wrong for test array index {}'.format(object_element_index))

        self.assertEquals(arr_or_rec.subindices, value_dict['subindices'],
                          'subindices wrong for test array index {}'.format(object_element_index))
        self.assertEquals(arr_or_rec.names, value_dict['names'],
                          'names wrong for test array index  {}'.format(object_element_index))

        self.assertEquals(len(arr_or_rec), len(value_dict['sub_objects']),
                          'number of sub objects does not match for test array index  {}'.format(object_element_index))

        for subvar_dict in value_dict['sub_objects']:
            self.assert_xdd_variable_equals(arr_or_rec.subindices[subvar_dict['subindex']], subvar_dict,
                                            'test array index {} subindex {}'.format(object_element_index,
                                                                                     subvar_dict['subindex']))

    @staticmethod
    def build_var_dict(index, subindex, name=None, default=None, unit='', factor=1, offset=0, min=None, max=None, value=None, data_type=None, access_type=None, description='', value_descriptions={}, bit_definitions={}):
        return {
            'index': index,
            'subindex': subindex,
            'name': name,
            'default': default,
            'unit': unit,
            'factor': factor,
            'offset': offset,
            'min': min,
            'max': max,
            'value': value,
            'data_type': data_type,
            'access_type': access_type,
            'description': description,
            'value_descriptions': value_descriptions,
            'bit_definitions': bit_definitions
        }

    @staticmethod
    def build_child_object_dict(dict_labels, index):
        return {label: objectdictionary.Variable('', index, x) for x, label in enumerate(dict_labels)}