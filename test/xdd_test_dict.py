import os
from canopen import objectdictionary
from canopen.objectdictionary import datatypes as dt
from test import can_open_unit_test_case as t

XDD_PATH = os.path.join(os.path.dirname(__file__), 'sample.xdd')

SAMPLE_XDD_EXPECTED = [
    (0, objectdictionary.Variable,
     t.CanOpenUnitTestCase.build_var_dict(0x1000, 0x00, name='Device Type', default=0xFFFFFFFF, unit='none',
                                          data_type=dt.UNSIGNED32, value=0x11FFFFFF, access_type='ro',
                                          description='Device Type Denotation'),
     ),
    (1, objectdictionary.Variable,
     t.CanOpenUnitTestCase.build_var_dict(0x1001, 0x00, name='Error Register', default=0x00, min=0, max=0xFFFFFFFF,
                                          data_type=dt.UNSIGNED8, value=0x00, access_type='rw',
                                          description='Error Register Denotation'),
     ),
    (2, objectdictionary.Record, {
        'index': 0x1018,
        'name': 'Identity Object',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0,1,2,3,4], 0x1018),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(
            ["Number of entries","Vendor Id","Product Code","Revision number","Serial number"], 0x1018),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x1018, 0x00, name='Number of entries', default=4, min=1, max=4,
                                                 data_type=dt.UNSIGNED8, access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x1018, 0x01, name='Vendor Id', default=1, data_type=dt.UNSIGNED32,
                                                 access_type='ro', description='This is a description for Vendor Id'),
            t.CanOpenUnitTestCase.build_var_dict(0x1018, 0x02, name='Product Code', default=1, data_type=dt.UNSIGNED32,
                                                 access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x1018, 0x03, name='Revision number', default=1, data_type=dt.UNSIGNED32,
                                                 access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x1018, 0x04, name='Serial number', default=234, data_type=dt.UNSIGNED32,
                                                 access_type='ro'),
        ]
    }),
    (3, objectdictionary.Record, {
        'index': 0x1800,
        'name': 'Transmit PDO Communication Parameter 0',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0,1,2,3,4,5,6], 0x1800),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(
            ["Number of entries","COB ID","Transmission Type","Inhibit Time","Compatibility Entry","Event Timer","SYNC start value"], 0x1800),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x1800, 0x00, name='Number of entries', default=0x06,
                                                 min=0x02, max=0x06, data_type=dt.UNSIGNED8, access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x1800, 0x01, name='COB ID', default=0x185,
                                                 min=0x00000001, max=0xFFFFFFFF, data_type=dt.UNSIGNED32, access_type='rw'),
            t.CanOpenUnitTestCase.build_var_dict(0x1800, 0x02, name='Transmission Type', default=255,
                                                 data_type=dt.UNSIGNED8, access_type='rw'),
            t.CanOpenUnitTestCase.build_var_dict(0x1800, 0x03, name='Inhibit Time', default=0x0000,
                                                 data_type=dt.UNSIGNED16, access_type='rw'),
            t.CanOpenUnitTestCase.build_var_dict(0x1800, 0x04, name='Compatibility Entry', default=0,
                                                 data_type=dt.UNSIGNED8, access_type='rw'),
            t.CanOpenUnitTestCase.build_var_dict(0x1800, 0x05, name='Event Timer', default=0,
                                                 data_type=dt.UNSIGNED16, access_type='rw'),
            t.CanOpenUnitTestCase.build_var_dict(0x1800, 0x06, name='SYNC start value', default=0,
                                                 data_type=dt.UNSIGNED8, access_type='rw'),
        ]
    }),
    (4, objectdictionary.Record, {
        'index': 0x1a00,
        'name': 'Transmit PDO Mapping Parameter 0',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0,1,2,3,4,5], 0x1a00),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(
            ["Number of entries","PDO Mapping Entry","PDO Mapping Entry_2","PDO Mapping Entry_3","PDO Mapping Entry_4","PDO Mapping Entry_5"], 0x1a00),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x1a00, 0x00, name='Number of entries', data_type=dt.UNSIGNED8, min=0, max=8, access_type='rw', default=5),
            t.CanOpenUnitTestCase.build_var_dict(0x1a00, 0x01, name='PDO Mapping Entry', data_type=dt.UNSIGNED32, access_type='rw', default=0x60000110),
            t.CanOpenUnitTestCase.build_var_dict(0x1a00, 0x02, name='PDO Mapping Entry_2', data_type=dt.UNSIGNED32, access_type='rw', default=0x60010108),
            t.CanOpenUnitTestCase.build_var_dict(0x1a00, 0x03, name='PDO Mapping Entry_3', data_type=dt.UNSIGNED32, access_type='rw', default=0x60020110),
            t.CanOpenUnitTestCase.build_var_dict(0x1a00, 0x04, name='PDO Mapping Entry_4', data_type=dt.UNSIGNED32, access_type='rw', default=0x60030110),
            t.CanOpenUnitTestCase.build_var_dict(0x1a00, 0x05, name='PDO Mapping Entry_5', data_type=dt.UNSIGNED32, access_type='rw', default=0x60040108),
        ]
    }),
    (5, objectdictionary.Array, {
        'index': 0x6000,
        'name': 'Status',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0,1], 0x6000),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(["Num Sub Entries","TM Status"], 0x6000),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x6000, 0x00, name='Num Sub Entries', default=0x01,
                                                 data_type=dt.UNSIGNED8, access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x6000, 0x01, name='TM Status', default=1,data_type=dt.UNSIGNED16,
                                                 access_type='ro'),
        ]
    }),
    (6, objectdictionary.Array, {
        'index': 0x6001,
        'name': '8 Bit Temperature',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0,1], 0x6001),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(["Num Sub Entries","TM Radiator Temperature"], 0x6001),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x6001, 0x00, name='Num Sub Entries', default=0x01, value=0xff,
                                                 data_type=dt.UNSIGNED8, access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x6001, 0x01, name='TM Radiator Temperature', default=0x08,
                                                 offset=-50, factor=2,
                                                 unit='degF',value=0x10,min=0x01,max=0x10,data_type=dt.UNSIGNED16,
                                                 access_type='rw',description='TM Radiator Temperature Denotation'),
        ]
    }),
    (7, objectdictionary.Array, {
        'index': 0x6002,
        'name': '16 Bit Temperature',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0, 1], 0x6002),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(["Num Sub Entries", "TM PCB Board Temperature"], 0x6002),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x6002, 0x00, name='Num Sub Entries', default=0x01,
                                                 data_type=dt.UNSIGNED8, access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x6002, 0x01, name='TM PCB Board Temperature', default=1, data_type=dt.UNSIGNED16,
                                                 access_type='ro',
                                                 offset=0, factor=1),
        ]
    }),
    (8, objectdictionary.Array, {
        'index': 0x6003,
        'name': '16 Bit Voltage',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0, 1], 0x6003),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(["Num Sub Entries", "Battery Voltage"], 0x6003),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x6003, 0x00, name='Num Sub Entries', default=0x01,
                                                 data_type=dt.UNSIGNED8, access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x6003, 0x01, name='Battery Voltage', default=1,
                                                 data_type=dt.UNSIGNED16,
                                                 access_type='ro', factor=0.1),
        ]
    }),
    (9, objectdictionary.Array, {
        'index': 0x6004,
        'name': 'Type',
        'subindices': t.CanOpenUnitTestCase.build_child_object_dict([0, 1], 0x6004),
        'names': t.CanOpenUnitTestCase.build_child_object_dict(["Num Sub Entries", "TM Type"], 0x6004),
        'sub_objects': [
            t.CanOpenUnitTestCase.build_var_dict(0x6004, 0x00, name='Num Sub Entries', default=0x01,
                                                 data_type=dt.UNSIGNED8, access_type='ro'),
            t.CanOpenUnitTestCase.build_var_dict(0x6004, 0x01, name='TM Type', default=1,
                                                 data_type=dt.UNSIGNED8,
                                                 access_type='ro', factor=0.1),
        ]
    }),
]

