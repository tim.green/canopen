import lxml.etree as et
from canopen import objectdictionary
from canopen.objectdictionary import xdd
from parameterized import parameterized
from test import xdd_test_dict as td
from test import can_open_unit_test_case as t


class TestCanopenObject(t.CanOpenUnitTestCase):

    def setUp(self):
        self.tree = et.parse(td.XDD_PATH).getroot()
        nsmap = self.tree.nsmap
        default_ns = nsmap.pop(None, None)
        nsmap['d'] = default_ns

        self.objects = self.tree.xpath('.//d:ISO15745Profile/d:ProfileBody[@xsi:type="ProfileBody_CommunicationNetwork_CANopen"]/d:ApplicationLayers/d:CANopenObjectList/d:CANopenObject', namespaces=nsmap)

    def test_constructor(self):
        canopen = xdd.CANopenObject(5, self.objects[0])
        self.assertIsNotNone(canopen)

    @parameterized.expand(td.SAMPLE_XDD_EXPECTED)
    def test_object(self, object_element_index, object_class, value_dict):
        can_open_object = xdd.CANopenObject(5, self.objects[object_element_index])

        variable = can_open_object.object

        self.assert_xdd_object_equals(variable, object_element_index, object_class, value_dict)

